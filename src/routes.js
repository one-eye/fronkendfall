import Home from './routes/Home';
import About from './routes/About';
import Rules from './routes/Rules';
import Tournaments from './routes/Tournaments';
import Tournament from './routes/Tournament';
import Register from './routes/Register';
import Archer from './routes/Archer';
import Admin from './routes/Admin';
import DisableUsers from './routes/DisableUsers';
import NewTournament from './routes/NewTournament';
import CreateTournament from './routes/CreateTournament';
import Casters from './routes/Casters';
import Login from './routes/Login';
import Participants from "./routes/Participants";
import ControlWrapper from "./routes/Control";
import Hud from "./routes/Hud";

const routes = [
  {
    path: '/',
    component: Home,
    exact: true
  },
  {
    path: '/login',
    component: Login,
    exact: true
  },
  {
    path: '/about',
    component: About,
    exact: true
  },
  {
    path: '/rules',
    component: Rules,
    exact: true
  },
  {
    path: '/tournaments',
    component: Tournaments,
    exact: true
  },
  {
    path: '/tournaments/new',
    component: NewTournament,
    exact: true,
    requireLogin: true,
    minUserLevel: 100,
  },
  {
    path: '/tournaments/new/:type',
    component: CreateTournament,
    exact: true,
    requireLogin: true,
    minUserLevel: 100,
  },
  {
    path: '/tournaments/:id',
    component: Tournament,
    exact: true
  },
  {
    path: '/tournaments/:id/casters',
    component: Casters,
    exact: true,
    requireLogin: true,
    minUserLevel: 100,
  },
  {
    path: '/tournaments/:id/control',
    component: ControlWrapper,
    exact: true,
    requireLogin: true,
    minUserLevel: 100,
  },
  {
    path: '/tournaments/:id/participants',
    component: Participants,
    exact: true,
    requireLogin: true,
    minUserLevel: 100,
  },
  {
    path: '/register',
    component: Register,
    exact: true
  },
  {
    path: '/archers/:id',
    component: Archer,
    exact: true
  },
  {
    path: '/admin',
    component: Admin,
    exact: true,
    requireLogin: true,
    minUserLevel: 100,
  },
  {
    path: '/admin/disable',
    component: DisableUsers,
    exact: true,
    requireLogin: true,
    minUserLevel: 100,
  },
  {
    path: '/live/hud',
    component: Hud,
    exact: true,
    requireLogin: false
  }
];

export default routes;
