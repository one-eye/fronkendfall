import {field, indexBy} from "./CollectionUtils";
import {Color} from "./Types";

export const colors = [
  { name: 'green', hex: '#4e9110' },
  { name: 'blue', hex: '#4c7cba' },
  { name: 'pink', hex: '#e39bb5' },
  { name: 'orange', hex: '#c75800' },
  { name: 'white', hex: '#fafafa' },
  { name: 'yellow', hex: '#efc822' },
  { name: 'cyan', hex: '#59c2c1' },
  { name: 'purple', hex: '#762c7a' },
  { name: 'red', hex: '#DB0F00' },
];

type HexBrand = {}
export type HexColor = string & HexBrand;

const colorsByName = indexBy(field('name'))(colors);

export const getHex = (color: Color): HexColor => colorsByName[color].hex;