import {isEnded, isStarted} from "./DateUtils";
import {Match, Player, PlayerSummary, Tournament, TournamentResponse} from "./Types";
import {Maybe} from "./Maybe";
import {isRunning as isMatchRunning} from "./MatchUtils";

const isRunning = (tournament: Tournament): boolean => isStarted(tournament) && !isEnded(tournament);

const getRunningTournament = (tournaments: Tournament[]): Maybe<Tournament> => new Maybe<Tournament>(tournaments.filter(t => t.ended === null).sort((t1, t2) => t2.id - t1.id)[0]);

const isTestTournament = (tournament: Tournament): boolean => !tournament.name.startsWith('DrunkenFall');

const toLocaleDate = (date: string | number | Date): string => new Date(date).toLocaleDateString();

const getWinnerOfMatch = (match: Match): Player => match.players.sort((p1, p2) => p2.match_score - p1.match_score)[0];

const getFinalMatch = (tournament: TournamentResponse): Maybe<Match> => new Maybe<Match>(tournament.matches.find(match => match.kind === 'final'));

const isIdiotStats = (tournament: TournamentResponse): boolean => getFinalMatch(tournament).map(match => match.players.map(p => p.match_score).reduce((p, c) => p + c, 0) === 0).getOrElse(false);

const getPlayerWithHighestScore = (tournament: TournamentResponse): PlayerSummary => tournament.player_summaries.sort((p1, p2) => p2.score - p1.score)[0];

const getWinnerOfTournament = (tournament: TournamentResponse): Maybe<PlayerSummary|Player> => isIdiotStats ? new Maybe<PlayerSummary>(getPlayerWithHighestScore(tournament)) : getFinalMatch(tournament).map(getWinnerOfMatch);

const getLosersOfTournament = (tournament: TournamentResponse, winnerPersonId: number): PlayerSummary[] => tournament.player_summaries.filter(ps => ps.person_id !== winnerPersonId).sort((p1, p2) => p2.score - p1.score);

const getStatsOfPlayer = (tournament: TournamentResponse, personId: number): Maybe<PlayerSummary> => new Maybe<PlayerSummary>(tournament.player_summaries.find(ps => ps.person_id === personId));

const pointComparator = (p1: Player | PlayerSummary, p2: Player | PlayerSummary): number => (p2.kills - p2.self) - (p1.kills - p1.self);

const onlyLeagueTournaments = (tournaments: Tournament[]): Tournament[] => tournaments.filter(t => t.cover);

const getTitleFromFullName = (fullName: string): string => fullName.split(':').reverse()[0];

const nullifyGoZeroDate = (date: string): string | null => `${date}` === "0001-01-01T00:00:00Z" ? null : date;

const canStart = (tournament: Tournament): boolean => !isStarted(tournament);

const getNextMatch = (matches: Match[]): Maybe<Match> => new Maybe<Match>(matches.find(match => !isStarted(match)));

const getNextScheduledMatch = (matches: Match[]): Match => {
  const nextMatchIndex = matches.findIndex(match => !isStarted(match));
  return matches[nextMatchIndex + 1];
};

const isBetweenMatches = (tournament: TournamentResponse): boolean => {
  if (isEnded(tournament.tournament)) {
    return false;
  }

  // pre-first-match
  if (isStarted(tournament.tournament) && !isStarted(tournament.matches[0])) {
    return true;
  }

  return !tournament.matches.some(isMatchRunning);
};

const getCurrentMatch = (tournament: TournamentResponse): Maybe<Match> => {
  return new Maybe(tournament.matches.find(isEnded));
};

const currentMatchIsOngoing = (tournament: TournamentResponse): boolean => {
  return getCurrentMatch(tournament).map(isStarted).getOrElse(false);
};

export {
  getRunningTournament,
  isRunning,
  toLocaleDate,
  getWinnerOfMatch,
  getWinnerOfTournament,
  getLosersOfTournament,
  getStatsOfPlayer,
  pointComparator,
  isTestTournament,
  onlyLeagueTournaments,
  getTitleFromFullName,
  nullifyGoZeroDate,
  canStart,
  getNextMatch,
  getNextScheduledMatch,
  isBetweenMatches,
  getCurrentMatch,
  currentMatchIsOngoing,
}
