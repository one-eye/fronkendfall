const indexBy = (indexer: ((a: any) => any) ) => (collection: any[]) => collection.reduce((p, c) => {
  p[indexer(c)] = c;
  return p;
}, {});


function field<T>(name: keyof T) {
  return function (obj: T): T[keyof T] {
    return obj[name];
  }
}

export {
  indexBy,
  field,
};