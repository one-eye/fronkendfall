import {isEnded, isStarted} from "./DateUtils";
import {Match} from "./Types";

export const canStart = (match: Match) => !isStarted(match);

export const getLevelTitle = (match: Match) => {
  if (match.level === "twilight") {
    return "Twilight Spire"
  } else if (match.level === "kingscourt") {
    return "King's Court"
  } else if (match.level === "frostfang") {
    return "Frostfang Keep"
  } else if (match.level === "sunken") {
    return "Sunken City"
  } else if (match.level === "amaranth") {
    return "The Amaranth"
  }
  return match.level.charAt(0).toUpperCase() + match.level.slice(1)
};

export const isRunning = (match: Match): boolean => isStarted(match) && !isEnded(match);
