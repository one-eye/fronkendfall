export enum ArcherType {
    Default = 0,
    Alternate = 1,
}

export enum ArrowType {
    Normal,
    Bomb,
    SuperBomb,
    Laser,
    Bramble,
    Drill,
    Bolt,
    Toy,
    Feather,
    Trigger,
    Prism,
}

export enum KillType {
    Arrow,
    Explosion,
    Brambles,
    JumpedOn,
    Lava,
    Shock,
    SpikeBall,
    FallingObject,
    Squish,
    Curse,
    Miasma,
    Enemy,
    Chalice,
}

export enum UserLevel {
    Player = 10,
    Judge = 30,
    Commentator = 50,
    Producer = 100,
}

export type Color = "green" |
    "blue" |
    "pink" |
    "orange" |
    "white" |
    "yellow" |
    "cyan" |
    "purple" |
    "red";

export type Level = "sacred" |
"twilight" |
"backfire" |
"flight" |
"mirage" |
"thornwood" |
"frostfang" |
"moonstone" |
"kingscourt" |
"sunken" |
"towerforge" |
"ascension" |
"amaranth" |
"dreadwood" |
"darkfang" |
"cataclysm";

export type Player = {
    id: number;
    match_id: number;
    person_id: number;
    index: number;
    nick: string;
    color: Color;
    preferred_color: Color;
    archer_type: ArcherType;
    shots: number;
    sweeps: number;
    kills: number;
    self: number;
    match_score: number;
    total_score: number;
    display_names: string[];
}

export type PlayerSummary = {
    id: number;
    person_id: number;
    shots: number;
    sweeps: number;
    kills: number;
    self: number;
    matches: number;
    score: number;
    skill_score: number;
}

export type PlayState = {
    id: number;
    player_id: number;
    round_id: number;
    index: number;
    arrows: ArrowType[];
    shield: boolean;
    wings: boolean;
    hat: boolean;
    invisible: boolean;
    speed: boolean;
    alive: boolean;
    lava: boolean;
    killer: number;
    up: number;
    down: number;
}

export type Person = {
    id: number;
    person_id: string;
    name: string;
    nick: string;
    preferred_color: Color;
    archer_type: ArcherType;
    avatar_url: string;
    userlevel: UserLevel;
    disabled: boolean;
    display_names: string[];
}

export type MatchKind = "qualifying" | "playoff" | "final" | "special";

export type Match = {
    id: number;
    tournament_id: number;
    players: Player[];
    kind: MatchKind;
    index: number;
    length: number;
    pause: string;
    scheduled: string;
    started: string;
    ended: string;
    level: Level;
    ruleset: string;
}

export type Tournament = {
    id: number;
    name: string;
    slug: string;
    opened: string;
    scheduled: string;
    started: string;
    qualifying_end: string;
    ended: string;
    color: Color;
    cover: string;
    length: number;
    final_length: number;
}

export type TournamentResponse = {
    tournament: Tournament;
    matches: Match[];
    player_summaries: PlayerSummary[];
    runnerups: PlayerSummary[];
    player_states: PlayState[];
}