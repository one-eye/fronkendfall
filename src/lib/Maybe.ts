export class Maybe<T> {
    value: T | null;

    constructor(value: T | null | undefined) {
        if (value) {
            this.value = value;
        } else {
            this.value = null;
        }
    }

    map<R>(func: (value: T) => R): Maybe<R> {
        return new Maybe<R>(this.value === null ? null : func(this.value));
    }

    getOrElse(defaultValue: T): T {
        return this.value === null ? defaultValue : this.value;
    }

    getOrThrow(errorSupplier: () => Error): T {
        if (this.value !== null) {
            return this.value;
        }
        throw errorSupplier();
    }

    hasValue(): boolean {
        return this.value !== null;
    }
}