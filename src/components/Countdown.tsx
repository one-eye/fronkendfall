import React from "react";
import useCountdown from "../useCountdown";
import {toTimeString} from "../lib/DateUtils";

const Countdown = ({to}: {to: string}) => {
  const timeLeft = useCountdown(to);
  const eta = timeLeft === 0 ? "Soon™" : toTimeString(timeLeft);
  return (
    <span>{eta}</span>
  )
};

export default Countdown;