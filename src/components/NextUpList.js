import React from "react";
import { getStatsOfPlayer } from "../lib/TournamentUtils";
import { getHex } from "../lib/ColorUtils";
import { getArcherImage } from "../lib/ArcherImages";

const NextUpPlayer = ({color, nick, total_score, matches, archer_type}) => {
  const hex = getHex(color);
  return (
    <div className="player">
      <div className="avatar">
        <img
          src={getArcherImage({color, alternate: archer_type === 1})} alt={`${color} archer`}
          style={{borderColor: hex}}
        />
      </div>
      <div className="nick" style={{color: hex}}>{nick}</div>
      <div className="points">{total_score} pts</div>
      <div className="matches">{matches} m</div>
    </div>
  );
};

const NextUpList = ({tournament, nextMatch, ...rest}) => (
  <div {...rest}>
    {
      nextMatch.players.map(player => (
        <NextUpPlayer
          {...player}
          matches={getStatsOfPlayer(tournament, player.person_id).matches}
          key={player.id}
        />
      ))
    }
  </div>
)

export default NextUpList;