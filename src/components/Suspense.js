import React from "react";
import useData from "../useData";
import Error from './Error';

const Suspense = ({ children, loader, deps, propName, ...rest }) => {
  const [data, loading, error] = useData(loader, deps);
  return (
    <>
      {
        loading ? <div className="loading">Loading...</div> :
          error ? <Error error={error}>Something went terribly wrong.. :(</Error> :
            React.Children.map(children, child => React.cloneElement(child, { [propName]: data }))
      }
    </>
  );
}

export default Suspense;
