import React, { useContext } from 'react';
import AuthenticationContext from './AuthenticationContext';
import { Redirect } from "react-router-dom";


const protectedPage = (minUserLevel, WrappedComponent) => (props) => {
  const { user } = useContext(AuthenticationContext);

  return (
    user === undefined || user.userlevel < minUserLevel ? <Redirect to={{ pathname: "/login", state: { from: props.location } }} /> :
      <WrappedComponent {...props} loggedInUser={user} />
  )
};

export default protectedPage;
