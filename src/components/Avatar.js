import React from "react";
import Ribbon from "../components/Ribbon";
import styles from './Avatar.module.css';

const Avatar = ({ archer, className }) => (
  <div className={`${styles.archer || ""} ${className || ""}`}>
    <div className={styles.avatar}>
      <div className="image">
        <img src={archer.avatarUrl} alt={archer.nick} />
      </div>
      <Ribbon color={archer.preferredColor}>
        {archer.nick}
      </Ribbon>
    </div>
  </div>
);

export default Avatar;
