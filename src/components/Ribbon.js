import React from "react";
import './Ribbon.css';

const Ribbon = ({ children, color = '' }) => (
  <div className={`ribbon ${color}`}>
    <div className="content">
      {children}
    </div>
  </div>
);

export default Ribbon;
