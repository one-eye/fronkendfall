import React, { useContext } from 'react';
import './Authenticated.css';
import AuthenticationContext from './AuthenticationContext';

const Authenticated = ({ userLevel = 0, children, ifNot }: any) => { //TODO

  const { user, logIn, logOut } = useContext(AuthenticationContext);
  return (
    <>
      {
        user === undefined ? (ifNot && React.cloneElement(ifNot, { logIn })) :
          user.userlevel < userLevel ? (ifNot && ifNot) :
            React.Children.map(children, child => React.cloneElement(child, { user, logOut }))
      }
    </>
  )
}

export default Authenticated;
