import React from 'react';
import './Error.css';

const Error = ({ error, children }) => (error ? <div className="error-message">{children}</div> : null);


export default Error;
