import React from 'react';
import './UserPicker.css';
import {Person} from "../lib/Types";

const getFirstName = (name: string) => (name || "").split(' ')[0];


type PersonProps = {
  person: Person;
  onClick?: (person: Person) => void;
}

const PersonItem = ({ person, onClick = () => {}}: PersonProps) => (
  <div className="person" onClick={() => onClick(person)}>
    <img src={person.avatar_url} width="96" height="96" alt={person.nick} />
    <p className="name">{getFirstName(person.name) || person.nick}</p>
  </div>
);

type UserPickerProps = {
  users: Person[];
  onUserSelect?: (person: Person) => void;
  className?: string;
}

const UserPicker = ({ users, onUserSelect, className = "" }: UserPickerProps) => {
  return (
    <div className={`user-list ${className}`}>
      {
        users.map(p => <PersonItem key={p.id} person={p} onClick={onUserSelect} />)
      }
    </div>
  )
};
export default UserPicker;
