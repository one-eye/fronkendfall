import React from "react";
import { Route, Link as RouterLink } from "react-router-dom";
import "./NavigationBar.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGamepad, faList, faQuestion, faBeer, faCog, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import oem from "../img/oem.svg";
import Authenticated from "./Authenticated";

const Link = ({ to, children }) => (
  <Route path={to} children={
    ({ match }) => <RouterLink to={to} className={match ? "active" : ""}>{children}</RouterLink>
  } />
);

const SignInLink = () => (
  <RouterLink to="/login" className="log-in">Sign In</RouterLink>
);

const User = ({ user, logOut }) => {
  return (
    <>
      <RouterLink to="/settings" className="settings desktop-only"><FontAwesomeIcon icon={faCog} /></RouterLink>
      <RouterLink to={`/archers/${user.id}`} className="profile">
        <img src={user.avatarUrl} alt={user.nick} />
        <p className={`desktop-only ${user.preferredColor}`}>{user.nick}</p>
      </RouterLink>
      <button onClick={logOut} className="log-out desktop-only"><FontAwesomeIcon icon={faSignOutAlt} /></button>
    </>
  )
};

const NavigationBar = () => (
  <div id="navigation-bar">
    <div className="logo">
      <RouterLink to="/"><img alt="One-Eye" src={oem} /></RouterLink>
    </div>
    <div className="main-routes">
      <Link to="/tournaments">
        <FontAwesomeIcon icon={faGamepad} />
        Tournament
      </Link>
      <Link to="/rules">
        <FontAwesomeIcon icon={faList} />
        Rules
      </Link>
      <Link to="/about">
        <FontAwesomeIcon icon={faQuestion} />
        About
      </Link>
      <Authenticated userLevel="100">
        <Link to="/admin">
          <FontAwesomeIcon icon={faBeer} />
          Superpowers
        </Link>
      </Authenticated>
    </div>
    <div className="user">
      <Authenticated ifNot={<SignInLink />}>
        <User />
      </Authenticated>
    </div>
  </div>
);

export default NavigationBar;
