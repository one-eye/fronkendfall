import useServerAction from "../useServerAction";
import { faCheck, faSkullCrossbones, faSpinner } from "@fortawesome/free-solid-svg-icons";
import { IconButton } from "./Button";
import React from "react";

const ActionButton = ({ onClick, timer = 0, children, ...rest }) => {
  const [clickHandler, loading, error, success] = useServerAction(onClick);

  const icon = loading ? faSpinner : error ? faSkullCrossbones : success ? faCheck : rest.icon;
  const kids = loading ? "Loading..." : error ? "Oh no!" : success ? "Done!" : children;

  const color = error ? "danger" : loading ? "" : rest.color;

  return (
    <IconButton
      {...rest}
      color={color}
      icon={icon}
      onClick={clickHandler}
      loading={loading}
    >
      {
        kids
      }
    </IconButton>
  )
};

export default ActionButton;