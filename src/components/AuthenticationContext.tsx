import React, { useState } from 'react';
import { getLoggedInUser } from '../api/api';
import { useEffect } from 'react';
import {Person} from "../lib/Types";

export type Session = {
  user: Person | undefined;
  logIn: () => Promise<void>;
  logOut: () => void;
}

const dummy = {user: undefined, logIn: () => Promise.resolve(), logOut: () => {}};

const AuthenticationContext = React.createContext<Session>(dummy);

const AuthProvider = ({ children }: any) => { //TODO: find the correct type
  const [user, setUser] = useState<Person|undefined>(undefined);
  useEffect(() => {
    getLoggedInUser().then(user => setUser(user));
  }, [setUser]);
  const logIn = () => getLoggedInUser().then(user => { setUser(user) });
  const logOut = () => setUser(undefined);
  return (
    <AuthenticationContext.Provider value={{user, logIn, logOut }}>
      {children}
    </AuthenticationContext.Provider>
  )
}

export default AuthenticationContext;
export { AuthProvider };
