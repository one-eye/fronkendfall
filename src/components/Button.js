import React from "react";
import './Button.css';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const ButtonContainer = ({ onClick, disabled, children, color, ...other }) => {
  const className = `button ${color || ''}`;
  return (
    <>
      {
        disabled ?
          <div className={`disabled ${className}`} {...other}>{children}</div > :
          typeof onClick === 'string' ?
            other.type === 'external' ?
              <a href={onClick} className={className} {...other}>{children}</a>:
              <Link to={onClick} className={className} {...other}>{children}</Link> :
            other.type === "submit" ?
              <button type="submit" className={className} {...other}>{children}</button>
              :
              <button type={other.type || "button"} onClick={onClick} className={className} {...other}>{children}</button>
      }
    </>
  )
}

const Button = ({ icon, onClick, children, color, ...other }) => {
  return (
    <ButtonContainer onClick={onClick} color={color} disabled={other.disabled} {...other} >
      <div className="icon"><FontAwesomeIcon icon={icon} /></div>
      <p>
        {children}
      </p>
    </ButtonContainer>
  );
}

const IconButton = ({ icon, onClick, children, color, disabled, loading=false, ...other }) => {
  return (
    <ButtonContainer onClick={onClick} color={color} disabled={disabled} {...other} >
      <div className={`icon ${loading?'loading':''}`}><FontAwesomeIcon icon={icon} /></div>
      <div className={`overlay ${loading?'loading':''}`}>
        {children}
      </div>
    </ButtonContainer>
  );
}

export default Button;
export { IconButton };
