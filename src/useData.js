import { useState, useEffect } from "react";

const useData = (loader, deps) => {

  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    setError(false);
    setLoading(true);
    loader()
      .then(a => setData(a))
      .catch(e => {
        console.error(e);
        setError(true);
      })
      .finally(() => setLoading(false));
  }, deps);

  return [data, loading, error];
}

export default useData;



