import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './App.css';
import routes from "./routes";
import ScrollToTop from './ScrollToTop';
import NavigationBar from './components/NavigationBar';
import { AuthProvider } from './components/AuthenticationContext';
import protectedPage from './components/ProtectedPage';
import store from './store';
import ErrorBoundary from "./components/ErrorBoundary";
import withErrorBoundary from "./components/withErrorBoundary";

const App = () => {
  return (
    <Router>
      <>
        <AuthProvider>
          <NavigationBar />
          <ScrollToTop>
            <div className="content">
              <ErrorBoundary>
                <Switch>
                  {
                    routes.map((route, i) => {
                      const Component = withErrorBoundary(route.requireLogin ? protectedPage(route.minUserLevel, route.component) : route.component);
                      return (
                          <Route key={i} {...route} component={Component} />
                      )
                    })
                  }
                </Switch>
              </ErrorBoundary>
            </div>
          </ScrollToTop>
        </AuthProvider>
      </>
    </Router>
  );
}

export default () => (<Provider store={store}><App /></Provider>);
