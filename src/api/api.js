// const baseUrl = 'http://localhost';
// const baseApiUrl = `${baseUrl}:3000/api`;
import { getTitleFromFullName, nullifyGoZeroDate } from "../lib/TournamentUtils";

const baseApiUrl = `/api`;

const rejectIfNotOk = path => r => r.ok ? r : new Promise((_, reject) => reject(new Error(`Call to ${path} failed. ${r.status} (${r.statusText})`)))

const get = (path, options = {}) => fetch(`${baseApiUrl}${path}`, {
  ...options,
  credentials: 'include'
})
  .then(rejectIfNotOk(path))
  .then(r => r.json());

const postRaw = (path, body, options = {}) => fetch(`${baseApiUrl}${path}`, {
    ...options,
    method: 'POST',
    credentials: 'include',
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
    },
    body: JSON.stringify(body)
  })
  .then(rejectIfNotOk(path));

const post = (path, body, options) => postRaw(path, body, options)
  .then(r => r.json());

const del = (path, options = {}) => fetch(`${baseApiUrl}${path}`, {
  ...options,
  method: 'DELETE',
  credentials: 'include',
})
  .then(rejectIfNotOk(path))
  .then(r => r.json());

const sanitizeTournamentDetailObject = tournament => ({
  ...tournament,
  title: getTitleFromFullName(tournament.name),
  started: nullifyGoZeroDate(tournament.started),
  ended: nullifyGoZeroDate(tournament.ended)
});

const sanitizeMatchObject = match => ({
  ...match,
  started: nullifyGoZeroDate(match.started),
  ended: nullifyGoZeroDate(match.ended),
  scheduled: nullifyGoZeroDate(match.scheduled)
});

const getAllTournaments = () => {
  return get('/tournaments/')
    // .then(a => new Promise((resolve) => setTimeout(() => resolve(a), 2000)))
    .then(r => r.tournaments.map(sanitizeTournamentDetailObject));
};
/**
 * Returns tournaments in the current league.
 * A tournament is in the current league if it has a cover image.
 * TODO: Make the model aware of the leagues.
 */
const getCurrentLeagueTournaments = () => {
  return getAllTournaments()
    .then(tournaments => tournaments.filter(t => t.cover));
};

const getLatestLeagueTournament = () => {
  return getCurrentLeagueTournaments()
    .then(tournaments => tournaments[0]);
};

const getTournament = (id) => {
  return get(`/tournaments/${id}/`)
    .then(t => ({
      ...t,
      matches: t.matches.map(sanitizeMatchObject),
      tournament: sanitizeTournamentDetailObject(t.tournament) }));
};

const registerPlayer = (nick, name, color, isAlternate) => {
  const data = { nick, name, color, archer_type: isAlternate ? 1 : 0 };
  return post('/user/register/', data)
    .then(a => new Promise((resolve, reject) => setTimeout(() => reject(a), 2000)))
    .then(r => r.person);
};

const sorted = fn => list => {
  list.sort(fn);
  return list;
};

const sortedByName = sorted((p1, p2) => p1.name.localeCompare(p2.name));

const getPeople = () => {
  return get('/people/')
    .then(r => r.people)
    .then(p => p.map(r => ({ ...r, avatarUrl: r.avatar_url, preferredColor: r.preferred_color })))
    .then(p => sortedByName(p));
};

const getAllPeople = () => {
  return get('/people/?all=true')
    .then(r => r.people)
    .then(p => p.map(r => ({ ...r, avatarUrl: r.avatar_url, preferredColor: r.preferred_color })))
    .then(p => sortedByName(p));
};

const getArcher = (id) => {
  return getPeople()
    .then(allArchers => allArchers.filter(person => `${person.id}` === `${id}`))
    .then(filtered => {
      if (filtered.length === 1) {
        return filtered[0];
      }
      throw new Error(`Could not find person with id ${id}`);
    })
    .then(r => ({ ...r, avatarUrl: r.avatar_url, preferredColor: r.preferred_color }));
};

const getLeaderboard = () => {
  return get('/people/')
    .then(r => r.leaderboard);
};

const getArcherStats = (id) => {
  return getLeaderboard()
    .then(allPlaces => allPlaces.filter(person => `${person.person_id}` === `${id}`))
    .then(filtered => {
      if (filtered.length === 1) {
        return filtered[0];
      }
      throw new Error(`Could not find person with id ${id} in the leaderboard.`);
    })
    .then(r => ({ ...r, personId: r.person_id, skillScore: r.skill_score, totalScore: r.total_score }))
};

const getFakeTournamentName = () => get('/fake/name');


const createDrunkenfall = ({ name, color, cover, id, scheduled }) => {
  const data = { name, color, cover, id, scheduled };
  return post('/tournaments/', data);
};
const createGroupTournament = ({ name, color, id, scheduled }) => {
  const data = { name, color, id, scheduled };
  return post('/tournaments/', data);
};

const submitCasters = (tournamentId, casterIds) => {
  const data = { casters: casterIds };
  return postRaw(`/tournaments/${tournamentId}/casters/`, data);
};

const normalizeUserFields = r => ({ ...r, archerType: r.archer_type, avatarUrl: r.avatar_url, displayNames: r.display_names, personId: r.person_id, preferredColor: r.preferred_color });

const getLoggedInUser = () => get('/user/')
  .then(r => r.authenticated === false ? undefined : normalizeUserFields({ ...r, authenticated: true }));

const deleteTestTournaments = () => del(`/tournaments/`)
  .then(r => r.tournaments);

const startTournament = id => get(`/tournaments/${id}/start/`);

const toggleUser = id => get(`/user/disable/${id}`);

const toggleParticipant = (tournamentId, playerId) => get(`/tournaments/${tournamentId}/toggle/${playerId}`);

const getParticipants = id => get(`/tournaments/${id}/players/`).then(response => response.player_summaries);

const startMatch = (tournamentId, matchIndex) => post(`/tournaments/${tournamentId}/play/?index=${matchIndex}`)

export {
  // baseUrl,
  getAllTournaments,
  getCurrentLeagueTournaments,
  getLatestLeagueTournament,
  getTournament,
  registerPlayer,
  getArcher,
  getArcherStats,
  getPeople,
  getAllPeople,
  getFakeTournamentName,
  createDrunkenfall,
  createGroupTournament,
  submitCasters,
  getLoggedInUser,
  deleteTestTournaments,
  startTournament,
  toggleUser,
  toggleParticipant,
  getParticipants,
  startMatch,
};
