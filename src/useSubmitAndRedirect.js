import { useState } from "react";

const useSubmitAndRedirect = (submitter, createRedirectUrl) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [redirect, setRedirect] = useState('');
  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    setError(false);
    submitter()
      .then(createRedirectUrl)
      .then((redirectUrl) => {
        setSuccess(true);
        setTimeout(() => setRedirect(redirectUrl), 1000);
      })
      .catch((e) => {
        console.error(e);
        setError(true);
      })
      .finally(() => setLoading(false));
  }
  return [handleSubmit, loading, error, success, redirect];
}

export default useSubmitAndRedirect;
