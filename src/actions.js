import {
  getAllTournaments,
  deleteTestTournaments,
  getTournament,
  getAllPeople,
  toggleUser as toggleUserApi,
  getParticipants,
  toggleParticipant as toggleParticipantApi,
  startMatch as startMatchApi
} from "./api/api";
import { participants } from "./reducers";
import { scheduledComparator } from "./lib/DateUtils";

// duplicate that for less typing >:')
// export const ACTION = 'ACTION';

export const LOAD_TOURNAMENTS_REQUEST = 'LOAD_TOURNAMENTS_REQUEST';
export const LOAD_TOURNAMENTS_SUCCESS = 'LOAD_TOURNAMENTS_SUCCESS';
export const LOAD_TOURNAMENTS_ERROR = 'LOAD_TOURNAMENTS_ERROR';

export const LOAD_TOURNAMENT_REQUEST = 'LOAD_TOURNAMENT_REQUEST';
export const LOAD_TOURNAMENT_SUCCESS = 'LOAD_TOURNAMENT_SUCCESS';
export const LOAD_TOURNAMENT_ERROR = 'LOAD_TOURNAMENT_ERROR';

export const LOAD_CURRENT_TOURNAMENT_REQUEST = 'LOAD_CURRENT_TOURNAMENT_REQUEST';
export const LOAD_CURRENT_TOURNAMENT_SUCCESS = 'LOAD_CURRENT_TOURNAMENT_SUCCESS';
export const LOAD_CURRENT_TOURNAMENT_ERROR = 'LOAD_CURRENT_TOURNAMENT_ERROR';

export const CLEAR_TEST_TOURNAMENTS_REQUEST = 'CLEAR_TEST_TOURNAMENTS_REQUEST';
export const CLEAR_TEST_TOURNAMENTS_SUCCESS = 'CLEAR_TEST_TOURNAMENTS_SUCCESS';
export const CLEAR_TEST_TOURNAMENTS_ERROR = 'CLEAR_TEST_TOURNAMENTS_ERROR';

export const LOAD_PEOPLE_REQUEST = 'LOAD_PEOPLE_REQUEST';
export const LOAD_PEOPLE_SUCCESS = 'LOAD_PEOPLE_SUCCESS';
export const LOAD_PEOPLE_ERROR = 'LOAD_PEOPLE_ERROR';

export const TOGGLE_USER_REQUEST = 'TOGGLE_USER_REQUEST';
export const TOGGLE_USER_SUCCESS = 'TOGGLE_USER_SUCCESS';
export const TOGGLE_USER_ERROR = 'TOGGLE_USER_ERROR';

export const LOAD_PARTICIPANTS_REQUEST = 'LOAD_PARTICIPANTS_REQUEST';
export const LOAD_PARTICIPANTS_SUCCESS = 'LOAD_PARTICIPANTS_SUCCESS';
export const LOAD_PARTICIPANTS_ERROR = 'LOAD_PARTICIPANTS_ERROR';

export const TOGGLE_PARTICIPANT_REQUEST = 'TOGGLE_PARTICIPANT_REQUEST';
export const TOGGLE_PARTICIPANT_SUCCESS = 'TOGGLE_PARTICIPANT_SUCCESS';
export const TOGGLE_PARTICIPANT_ERROR = 'TOGGLE_PARTICIPANT_ERROR';

export const START_MATCH_REQUEST = 'START_MATCH_REQUEST';
export const START_MATCH_SUCCESS = 'START_MATCH_SUCCESS';
export const START_MATCH_ERROR = 'START_MATCH_ERROR';


export const clearTestTournamentsRequest = () => ({ type: CLEAR_TEST_TOURNAMENTS_REQUEST });
export const clearTestTournamentsSuccess = () => ({ type: CLEAR_TEST_TOURNAMENTS_SUCCESS });
export const clearTestTournamentsError = (error) => ({ type: CLEAR_TEST_TOURNAMENTS_ERROR, error });

export const loadTournamentsRequest = () => ({ type: LOAD_TOURNAMENTS_REQUEST });
export const loadTournamentsSuccess = (tournaments) => ({ type: LOAD_TOURNAMENTS_SUCCESS, tournaments });
export const loadTournamentsError = (error) => ({ type: LOAD_TOURNAMENTS_ERROR, error });

export const loadTournamentRequest = (id) => ({ type: LOAD_TOURNAMENT_REQUEST, id });
export const loadTournamentSuccess = (tournament) => ({ type: LOAD_TOURNAMENT_SUCCESS, tournament });
export const loadTournamentError = (error) => ({ type: LOAD_TOURNAMENT_ERROR, error });

export const loadCurrentTournamentRequest = () => ({ type: LOAD_CURRENT_TOURNAMENT_REQUEST });
export const loadCurrentTournamentSuccess = (tournamentId) => ({ type: LOAD_CURRENT_TOURNAMENT_SUCCESS, tournamentId });
export const loadCurrentTournamentError = (error) => ({ type: LOAD_CURRENT_TOURNAMENT_ERROR, error });

export const loadPeopleRequest = () => ({ type: LOAD_PEOPLE_REQUEST });
export const loadPeopleSuccess = (people) => ({ type: LOAD_PEOPLE_SUCCESS, people });
export const loadPeopleError = (error) => ({ type: LOAD_PEOPLE_ERROR, error });

export const toggleUserRequest = (id) => ({ type: TOGGLE_USER_REQUEST, id });
export const toggleUserSuccess = (id) => ({ type: TOGGLE_USER_SUCCESS, id });
export const toggleUserError = (error) => ({ type: TOGGLE_USER_ERROR, error });

export const loadParticipantsRequest = (id) => ({ type: LOAD_PARTICIPANTS_REQUEST, id });
export const loadParticipantsSuccess = (id, participants) => ({ type: LOAD_PARTICIPANTS_SUCCESS, id, participants });
export const loadParticipantsError = (id, error) => ({ type: LOAD_PARTICIPANTS_ERROR, id, error });

export const toggleParticipantRequest = (tournamentId, playerId) => ({ type: TOGGLE_PARTICIPANT_REQUEST, tournamentId, playerId });
export const toggleParticipantSuccess = (tournamentId, playerId) => ({ type: TOGGLE_PARTICIPANT_SUCCESS, tournamentId, playerId });
export const toggleParticipantError = (error) => ({ type: TOGGLE_PARTICIPANT_ERROR, error });

export const fetchTournaments = () => dispatch => {
  dispatch(loadTournamentsRequest());
  getAllTournaments().then(
    tournaments => dispatch(loadTournamentsSuccess(tournaments)),
    error => dispatch(loadTournamentsError(error))
  )
};

export const fetchTournament = (id) => dispatch => {
  dispatch(loadTournamentRequest(id));
  getTournament(id).then(
    tournament => dispatch(loadTournamentSuccess(tournament)),
    error => dispatch(loadTournamentError(error))
  )
};

export const fetchCurrentTournament = () => dispatch => {
  dispatch(loadCurrentTournamentRequest());
  getAllTournaments().then(
    tournaments => {
      const currentTournament = tournaments.sort(scheduledComparator)[0];
      dispatch(loadCurrentTournamentSuccess(currentTournament.id));
      dispatch(fetchTournament(currentTournament.id));
    },
    error => {
      dispatch(loadTournamentsError(error));
      dispatch(loadCurrentTournamentError(error));
    }
  )
};

export const fetchPeople = () => dispatch => {
  dispatch(loadPeopleRequest());
  getAllPeople()
    .then(
      people => dispatch(loadPeopleSuccess(people)),
      error => dispatch(loadPeopleError(error))
    )
};

export const toggleUser = (id) => dispatch => {
  dispatch(toggleUserRequest(id));
  toggleUserApi(id)
    .then(
      () => {
        dispatch(fetchPeople());
        dispatch(toggleUserSuccess(id))
      },
      error => dispatch(toggleUserError(error)))
};

export const clearTestTournaments = () => dispatch => {
  dispatch(clearTestTournamentsRequest());
  deleteTestTournaments().then(
    r => {
      dispatch(clearTestTournamentsSuccess());
      dispatch(loadTournamentsSuccess(r))
    },
    error => dispatch(clearTestTournamentsError(error))
  )
};

export const fetchParticipants = (id) => dispatch => {
  dispatch(loadParticipantsRequest(id));
  getParticipants(id).then(
    participants => dispatch(loadParticipantsSuccess(id, participants)),
    error => dispatch(loadParticipantsError(id, error))
  )
};

export const toggleParticipant = (tournamentId, playerId,) => dispatch => {
  dispatch(toggleParticipantRequest(tournamentId, playerId));
  toggleParticipantApi(tournamentId, playerId)
    .then(
      () => {
        dispatch(fetchParticipants(tournamentId));
        dispatch(toggleParticipantSuccess(tournamentId, playerId))
      },
      error => dispatch(toggleParticipantError(error)))
};
