import * as actions from "./actions";
import { field, indexBy } from "./lib/CollectionUtils";

const tournamentsInitialState = {
  isLoading: false,
  error: null,
  data: []
};

export const tournaments = (state = tournamentsInitialState, action) => {
  switch (action.type) {
    case actions.LOAD_TOURNAMENTS_REQUEST:
      return Object.assign({}, state, { isLoading: true, error: null });
    case actions.LOAD_TOURNAMENTS_SUCCESS:
      return Object.assign({}, state, { isLoading: false, data: action.tournaments });
    case actions.LOAD_TOURNAMENTS_ERROR:
      return Object.assign({}, state, { isLoading: false, error: action.error });
    case actions.CLEAR_TEST_TOURNAMENTS_REQUEST:
      return Object.assign({}, state, { isLoading: true, error: null });
    case actions.CLEAR_TEST_TOURNAMENTS_SUCCESS:
      return Object.assign({}, state, { isLoading: false, error: null });
    case actions.CLEAR_TEST_TOURNAMENTS_ERROR:
      return Object.assign({}, state, { isLoading: false, error: action.error });
    default:
      return state;
  }
};

const tournamentDetailsInitialState = {
  isLoading: false,
  error: null,
  data: {}
};

export const tournamentDetails = (state = tournamentDetailsInitialState, action) => {
  switch (action.type) {
    case actions.LOAD_TOURNAMENT_REQUEST:
      return Object.assign({}, state, { isLoading: true, error: null, data: {...state.data, [action.id]: null}});
    case actions.LOAD_TOURNAMENT_SUCCESS:
      return Object.assign({}, state, { isLoading: false, data: {...state.data, [action.tournament.tournament.id]: action.tournament} });
    case actions.LOAD_TOURNAMENT_ERROR:
      return Object.assign({}, state, { isLoading: false, error: action.error });
    default:
      return state;
  }
};

const currentTournamentIdInitialState = {
  isLoading: false,
  error: null,
  tournamentId: null,
};

export const currentTournamentId = (state = currentTournamentIdInitialState, action) => {
  switch (action.type) {
    case actions.LOAD_CURRENT_TOURNAMENT_REQUEST:
      return Object.assign({}, state, { isLoading: true, error: null, tournamentId: null});
    case actions.LOAD_CURRENT_TOURNAMENT_SUCCESS:
      return Object.assign({}, state, { isLoading: false, error: null, tournamentId: action.tournamentId});
    case actions.LOAD_CURRENT_TOURNAMENT_ERROR:
      return Object.assign({}, state, { isLoading: false, error: action.error, tournamentId: null});
    default:
      return state;
  }
};

const playersInitialState = {
  isLoading: false,
  error: null,
  data: {}
};
export const players = (state = playersInitialState, action) => {
  switch (action.type) {
    case actions.LOAD_PEOPLE_REQUEST:
      return Object.assign({}, state, { isLoading: true, error: null });
    case actions.LOAD_PEOPLE_SUCCESS:
      return Object.assign({}, state, { isLoading: false, error: null, data: indexBy(field('id'))(action.people) });
    case actions.LOAD_PEOPLE_ERROR:
      return Object.assign({}, state, { isLoading: false, error: action.error });
    default:
      return state;
  }
};

const participantsInitialState = {
  isLoading: false,
  error: null,
  data: {}
};

export const participants = (state = participantsInitialState, action) => {
  switch (action.type) {
    case actions.LOAD_PARTICIPANTS_REQUEST:
      return Object.assign({}, state, {isLoading: true, error: null});
    case actions.LOAD_PARTICIPANTS_SUCCESS:
      return Object.assign({}, state, {isLoading: false, error: null, data: Object.assign({}, state.data, {[action.id]: action.participants})});
    case actions.LOAD_PARTICIPANTS_ERROR:
      return Object.assign({}, state, {isLoading: false, error: action.error});
    default:
      return state;
  }
};