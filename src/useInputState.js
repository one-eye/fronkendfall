import { useState } from "react";

const useInputState = (initial) => {
  const [state, setState] = useState(initial);
  const handleChange = (e) => setState(e.target.value);
  return [state, handleChange, setState];
}

export default useInputState;
