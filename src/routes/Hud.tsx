import React, {useEffect} from "react";
import { connect } from "react-redux";
import {TournamentResponse} from "../lib/Types";
import {currentMatchIsOngoing, isBetweenMatches} from "../lib/TournamentUtils";
import {isEnded, isStarted} from "../lib/DateUtils";
import Loading from "../components/Loading";
import {fetchCurrentTournament, fetchParticipants, fetchPeople, fetchTournament, toggleParticipant} from "../actions";

interface HudContentsProps {
  tournament: TournamentResponse;
}

type HudState = "pre-tournament" | "between-matches" | "ongoing-match" | "post-tournament";

const getHudState = (tournament: TournamentResponse): HudState => {
  if (!isStarted(tournament.tournament)) {
    return "pre-tournament";
  }
  if (isBetweenMatches(tournament)) {
    return "between-matches";
  }
  if (!isEnded(tournament.tournament) && currentMatchIsOngoing(tournament)) {
    return "ongoing-match";
  }
  return "post-tournament";
};

const Contents: React.FunctionComponent<HudContentsProps> = ({tournament}) => {
  switch (getHudState(tournament)) {
    case "pre-tournament":
      return <h1>It's about to start!</h1>;
    case "between-matches":
      return <h1>Between matches.</h1>;
    case "ongoing-match":
      return <h1>Ongoing match</h1>;
    case "post-tournament":
      return <h1>Someone won</h1>;
  }
};

interface HudProps {
  tournament?: TournamentResponse;
  isLoading: boolean;
  error?: Error;
  loadCurrentTournament: () => {};
  loadUsers: () => {};
  loadParticipants: (a: number) => {};
}
const Hud: React.FunctionComponent<HudProps> = ({tournament, isLoading, error, loadParticipants, loadUsers, loadCurrentTournament}) => {
  const tournamentId = tournament ? tournament.tournament.id : undefined;
  useEffect(() => {
    loadCurrentTournament();
    loadUsers();
  }, []);
  useEffect(() => {
    tournamentId && loadParticipants(tournamentId);
  }, [tournamentId]);
  return (
    <div id="hud">
      <Loading isLoading={isLoading} error={error}>
        {
          tournament && <Contents tournament={tournament}/>
        }
      </Loading>
    </div>
  );
};

// TODO type this shit
const mapStateToProps = (state: any, ownProps: any) => ({
  people: state.players.data,
  participants: state.participants.data[state.currentTournamentId.tournamentId] || [],
  loading: state.currentTournamentId.loading || state.tournamentDetails.loading || state.players.loading || state.participants.loading,
  error: state.currentTournamentId.loading || state.tournamentDetails.error || state.players.error || state.participants.error,
  tournament: state.tournamentDetails.data[state.currentTournamentId.tournamentId]
});

const mapDispatchToProps = (dispatch: any, props: any) => ({
  loadCurrentTournament: () => dispatch(fetchCurrentTournament()),
  loadUsers: () => dispatch(fetchPeople()),
  loadParticipants: (tournamentId: number) => dispatch(fetchParticipants(tournamentId)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Hud);