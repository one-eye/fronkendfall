import React, { useState, useContext, useEffect } from 'react';
import NextUpList from "../components/NextUpList";
import { connect } from "react-redux";
import { getNextMatch} from "../lib/TournamentUtils";
import AuthenticationContext from "../components/AuthenticationContext";
import { fetchTournament } from "../actions";
import Loading from "../components/Loading";
import TournamentControls from "../components/TournamentControls";
import { faBaby, faPlay, faRadiation, faRecycle } from "@fortawesome/free-solid-svg-icons";
import Button, { IconButton } from "../components/Button";

import './Control.css';
import { startMatch } from "../api/api";
import ActionButton from "../components/ActionButton";
import { getLevelTitle } from "../lib/MatchUtils";
import Countdown from "../components/Countdown";


const Control = ({tournament, startMatch}) => {
  const [isDangerous, setDangerous] = useState(false);
  const nextMatch = getNextMatch(tournament.matches)
    .getOrThrow(() => new Error('Control: Could not get next match.'));
  const { user } = useContext(AuthenticationContext);

  return (
    <>
      <TournamentControls tournament={tournament} user={user} />
      <div id="control-page">
        <div className="match-title">
          {nextMatch.kind} - {"Round -1"} @ {getLevelTitle(nextMatch)}
        </div>
        <div className="god-button">
          <ActionButton
            icon={faPlay}
            color="positive"
            onClick={() => startMatch(nextMatch.index)}
          />
        </div>
        <div className="scheduled-at">
          Scheduled at <Countdown to={nextMatch.scheduled} />
        </div>
        <div className="next-up">
          <NextUpList className="players" tournament={tournament} nextMatch={nextMatch} />
        </div>
        <div className="danger-zone">
          {
            isDangerous ?
              <Button icon={faBaby} color="positive" onClick={() => setDangerous(false)}>Hide Dangerous Commands</Button>:
              <Button icon={faRadiation} color="danger" onClick={() => setDangerous(true)}>Show Dangerous Commands</Button>
          }
          {
            isDangerous ?
              <div className="actions">
                <Button icon={faRecycle} onClick={() => console.error('Not implemented')}>Reset {nextMatch.kind}</Button>
              </div>
              : null
          }
        </div>
      </div>
    </>
  )
};

const ControlWrapper = ({match: { params: { id: tournamentId }}, tournament, isLoading, error, loadTournament}) => {
  useEffect(() => { loadTournament(); }, []);
  return (
    <Loading isLoading={isLoading || !tournament} error={error}>
      <Control tournament={tournament} startMatch={(matchIndex) => startMatch(tournamentId, matchIndex)}/>
    </Loading>
  );
};

const mapStateToProps = (state, props) => ({
  tournament: state.tournamentDetails.data[props.match.params.id],
  isLoading: state.tournamentDetails.isLoading,
  error: state.tournamentDetails.error,
});
const mapDispatchToProps = (dispatch, props) => ({
  loadTournament: () => dispatch(fetchTournament(props.match.params.id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ControlWrapper)