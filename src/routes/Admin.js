import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { faBalanceScale, faPlus, faUserPlus, faBan, faFire, faFlag, faForward, faTrophy, faCamera, faTrashAlt, faBullhorn } from '@fortawesome/free-solid-svg-icons'

import './Admin.css';
import Button from '../components/Button';
import { getRunningTournament, isTestTournament } from '../lib/TournamentUtils';
import { clearTestTournaments, fetchTournaments } from '../actions';
import Loading from '../components/Loading';

const testTournamentsExist = tournaments => tournaments.some(isTestTournament);


const GodButton = ({tournament}) => (
  <div className="section">
    <h1>God button</h1>
    <div className="button-list">
      <Button icon={faBalanceScale} onClick={`/tournaments/${tournament.id}/control`}>
        God button
      </Button>
    </div>
  </div>
);

const Admin = ({ tournaments, loadTournaments, onClearTests, isLoading, error }) => {
  useEffect(() => loadTournaments(), []);
  const canClearTests = testTournamentsExist(tournaments);
  const maybeRunningTournament = getRunningTournament(tournaments);

  return (
    <div id="admin-page">
      {
        maybeRunningTournament.map(runningTournament => (
          <GodButton tournament={runningTournament} />
        )).getOrElse(null)
      }
      <div className="section">
        <h1>Users</h1>
        <div className="button-list">
          <Button icon={faUserPlus} color="positive" onClick="/register">Register users</Button>
          <Button icon={faBan} color="danger" onClick="/admin/disable">Disable users</Button>
        </div>
      </div>
      <div className="section">
        <h1>Tournament management</h1>
        <div className="button-list">
          <Button icon={faPlus} color="positive" onClick="/tournaments/new">New tournament</Button>
          <Button icon={faTrashAlt} color="danger" onClick={onClearTests} disabled={!canClearTests}>Clear tests</Button>
        </div>
      </div>
      <div className="section">
        <h1>Testing</h1>
        <div className="button-list">
          <Button icon={faFire} onClick="/live/game">Game screen</Button>
          <Button icon={faFlag} onClick="/live/qualifications">Qualifications game screen</Button>
          <Button icon={faForward} onClick="/live/next">Next screen</Button>
          <Button icon={faBullhorn} onClick="/live/overview">Tournament overview</Button>
          <Button icon={faTrophy} onClick="" disabled={true}>Leaderboard</Button>
          <Button icon={faCamera} onClick="/live/hud">HUD</Button>
        </div>
      </div>
      <div className="section">
        <h1>Tournaments</h1>
        <div className="tournament-list">
          <Loading isLoading={isLoading} error={error}>
            {
              tournaments.map(tournament =>
                <Link to={`/tournaments/${tournament.id}`} key={tournament.id}>
                  {tournament.cover && <img src={tournament.cover} alt={tournament.name} />}
                  <div>{tournament.name}</div>
                </Link>)
            }
          </Loading>
        </div>
      </div>
    </div>
  )
};

const mapStateToProps = state => ({
  tournaments: state.tournaments.data,
  isLoading: state.tournaments.isLoading,
  error: state.tournaments.error
});
const mapDispatchToProps = dispatch => ({
  onClearTests: () => dispatch(clearTestTournaments()),
  loadTournaments: () => dispatch(fetchTournaments())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Admin);
