import React, { useContext } from 'react';
import useTitle from "../useTitle";
import TournamentControls from "../components/TournamentControls";
import {isoStringToHumanString, isToday, toTimeString} from "../lib/DateUtils";

import './ScheduledTournament.css';
import {Person, PlayerSummary, TournamentResponse} from "../lib/Types";
import AuthenticationContext, {Session} from "../components/AuthenticationContext";
import {faTimes, faUserPlus} from "@fortawesome/free-solid-svg-icons";
import Button from "../components/Button";
import Ribbon from "../components/Ribbon";
import Countdown from "../components/Countdown";

const renderLeaveButton = (user: Person, toggleParticipant: (personId: number) => void) => (
  <>
    <h2>You're in! Much Excite!</h2>
    <Button
      icon={faTimes}
      color="warning"
      onClick={() => toggleParticipant(user.id)}
    >Leave tournament</Button>
  </>
);

const renderJoinButton = (user: Person, toggleParticipant: (personId: number) => void) => (
  <>
    <h2>Join the showdown!</h2>
    <Button
      icon={faUserPlus}
      color="positive"
      onClick={() => toggleParticipant(user.id)}
    >Yes Please!</Button>
  </>
);

const renderDateOrCountdown = (isoDate: string) => isToday(isoDate) ? <Countdown to={isoDate}/> : isoStringToHumanString(isoDate);


interface ScheduledTournamentProp {
  tournament: TournamentResponse;
  people: Person[];
  participants: PlayerSummary[];
  toggleParticipant: (personId: number) => void;
}

const ScheduledTournament = ({tournament, people, participants, toggleParticipant}: ScheduledTournamentProp) => {
  useTitle(`${tournament.tournament.name} - DrunkenFall`);
  const { user }: Session = useContext(AuthenticationContext);
  const participatingUsers = participants.map(p => people[p.person_id]).filter(p => !!(p));
  const userIsParticipating = (user !== undefined) && participatingUsers.some(p => p.person_id === user.person_id);

  return (
    <>
      <TournamentControls tournament={tournament} user={user}/>
      <div id="scheduled-tournament-page">
        <div className="title">
          <h1>{tournament.tournament.name}</h1>
          <p>{renderDateOrCountdown(tournament.tournament.scheduled)}</p>
        </div>
        <div className="participation-button-container">
          {
            user === undefined ? null
              : userIsParticipating ?
                renderLeaveButton(user, toggleParticipant)
                :
                renderJoinButton(user, toggleParticipant)
          }
        </div>
        <div className="participants">
          {
            participatingUsers.reverse().map(person => (
              <div key={person.person_id} className="person">
                <img src={person.avatar_url} width="96" height="96" alt={person.nick} />
              </div>
            ))
          }
        </div>
        <div className="ribbon-container">
          <Ribbon>
            {renderDateOrCountdown(tournament.tournament.scheduled)}
          </Ribbon>
        </div>
      </div>
    </>
  );
};
export default ScheduledTournament;

