import React from "react";
import { getArcher, getArcherStats } from "../api/api";
import './Archer.css';
import Suspense from "../components/Suspense";
import Avatar from '../components/Avatar';

const Stats = ({ stats }) => {
  return (
    <table className="GRABABRUSHANDPUTONALITTLETABLE">
      <tbody>
        <tr><td>Kills</td><td>{stats.kills}</td></tr>
        <tr><td>Matches</td><td>{stats.matches}</td></tr>
        <tr><td>Self kills</td><td>{stats.self}</td></tr>
        <tr><td>Shots</td><td>{stats.shots}</td></tr>
        <tr><td>Skill Score</td><td>{stats.skillScore}</td></tr>
        <tr><td>Sweeps</td><td>{stats.sweeps}</td></tr>
        <tr><td>Total Score</td><td>{stats.totalScore}</td></tr>
        <tr><td>Tournaments played</td><td>{stats.tournaments}</td></tr>
      </tbody>
    </table>
  );
}

const Archer = ({ match: { params: { id } }, location }) => {
  const registered = location.search.includes('register=true');

  return (
    <div id="archer-page">
      <Suspense loader={() => getArcher(id)} deps={[id]} propName="archer">
        <Avatar />
      </Suspense>
      {
        registered ?
          <>
            <h1>Congration</h1>
            <p>You have been assigned a random avatar!</p>
          </>
          :
          <Suspense loader={() => getArcherStats(id)} deps={[id]} propName="stats">
            <Stats />
          </Suspense>
      }
    </div>
  )
}

export default Archer;
