import React, { useState } from "react";
import './CreateTournament.css';
import { Redirect } from "react-router-dom";
import Suspense from '../components/Suspense';
import { getFakeTournamentName, createDrunkenfall, createGroupTournament } from "../api/api";
import useInputState from "../useInputState";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import ColorPicker from '../components/ColorPicker';
import useSubmitAndRedirect from "../useSubmitAndRedirect";
import SubmitButton from "../components/SubmitButton";
import Error from '../components/Error';

const DRUNKENFALL = 'drunkenfall';

const empty = (thing) => (!thing || !(thing.trim()));

const validate = (name, id, cover, scheduled, type) => {
  if (
    empty(name)
    || empty(id)
    || empty(scheduled)
    || (empty(cover) && type === DRUNKENFALL)
  ) {
    return false;
  }
  return true;
}

const iso = dateStr => new Date(dateStr).toISOString();

const Form = ({ fakeName, type }) => {

  const [color, setColor] = useState({ name: "green", hex: "#4e9110" });
  const [name, handleNameChange] = useInputState(fakeName.name);
  const [id, handleIdChange] = useInputState(fakeName.numeral);
  const [cover, handleCoverChange] = useInputState('');
  const [date, handleDateChange] = useInputState('');

  const submitter = type === DRUNKENFALL ? createDrunkenfall : createGroupTournament;

  const [handleSubmit, loading, error, success, redirect] = useSubmitAndRedirect(
    () => submitter({ color: color.name, id, name, cover, scheduled: iso(date) }),
    (response) => `/tournaments/${response.id}`
  );

  const isValid = validate(name, id, cover, date, type);
  const headerStyle = { color: color.hex };
  return (
    <>
      {
        redirect && <Redirect to={redirect} />
      }
      <h1 style={headerStyle}>{name}</h1>
      <ColorPicker value={color} onChange={setColor} />
      <form onSubmit={handleSubmit}>
        <div className="field">
          <label htmlFor="name">Name</label>
          <input type="text" name="name" id="name" value={name} onChange={handleNameChange} data-lpignore='true' />
        </div>
        <div className="field">
          <label htmlFor="id">ID</label>
          <input type="text" name="id" id="id" value={id} onChange={handleIdChange} data-lpignore='true' />
        </div>
        {
          type === DRUNKENFALL &&
          <div className="field">
            <label htmlFor="cover">Cover</label>
            <input type="text" name="cover" id="cover" value={cover} onChange={handleCoverChange} data-lpignore='true' />
          </div>
        }
        <div className="field">
          <label htmlFor="scheduled">Scheduled to start at</label>
          <input type="datetime-local" name="scheduled" id="scheduled" value={date} onChange={handleDateChange} />
        </div>
        <Error error={error}>Something went terribly wrong.. :(</Error>
        <SubmitButton icon={faPlus} disabled={!isValid} color="positive" {...{ loading, error, success }}>
          Create
        </SubmitButton>
      </form>
    </>
  )
}

const CreateTournament = ({ match: { params: { type } } }) => (

  <div id="create-tournament-page">
    <Suspense loader={() => getFakeTournamentName()} deps={[]} propName="fakeName" >
      <Form type={type} />
    </Suspense>
  </div>
);

export default CreateTournament;
