import React, { useState } from 'react';
import './Casters.css';
import { getPeople, submitCasters, getTournament } from "../api/api";
import UserPicker from '../components/UserPicker';
import Suspense from '../components/Suspense';
import SubmitButton from '../components/SubmitButton';
import useSubmitAndRedirect from '../useSubmitAndRedirect';
import { faCloudUploadAlt } from '@fortawesome/free-solid-svg-icons'
import useData from '../useData';
import useTitle from '../useTitle';
import TournamentControls from '../components/TournamentControls';
import Error from '../components/Error';
import { byName } from "../lib/UserUtils";

const CasterSelection = ({ users, tournamentId }) => {

  const [casters, setCasters] = useState([]);
  const [sheeple, setSheeple] = useState(users);

  const addCaster = (user) => {
    if (casters.length < 2) {
      setCasters(casters => [...casters, user]);
      setSheeple(sheeple => sheeple.filter(sheep => sheep.id !== user.id))
    }
  };
  const removeCaster = (user) => {
    setCasters(casters => casters.filter(caster => caster.id !== user.id));
    setSheeple(sheeple => [...sheeple, user].sort(byName));
  };

  const [handleSubmit, loading, error, success] = useSubmitAndRedirect(
    () => submitCasters(tournamentId, casters.map(c => c.id)),
    () => '?yeah'
  );

  return (
    <form onSubmit={handleSubmit}>
      <Error error={error}>Something went terribly wrong.. :(</Error>
      <SubmitButton
        icon={faCloudUploadAlt}
        disabled={casters.length !== 2}
        {...{ loading, success }}
      >
        Set
      </SubmitButton>
      <h1>Casters ({casters.length}/2)</h1>
      <UserPicker users={casters} onUserSelect={removeCaster} />
      <h1>Sheeple</h1>
      <UserPicker users={sheeple} onUserSelect={addCaster} />
    </form>
  )
};

const Casters = ({ match: { params: { id: tournamentId } }, loggedInUser}) => {
  const [tournament, loading, error] = useData(() => getTournament(tournamentId), [tournamentId]);
  const title = loading ? 'Loading' : error ? 'Drunkenfall' : `${tournament.tournament.title} / Casters - Drunkenfall`;
  useTitle(title);
  return (
    <div id="casters-page">
      {
        !(loading || error) && <TournamentControls tournament={tournament} user={loggedInUser} />
      }
      //FIXME: use redux instead
      <Suspense loader={() => getPeople()} deps={[]} propName="users" >
        <CasterSelection tournamentId={tournamentId} />
      </Suspense>
    </div>
  )
};

export default Casters;
