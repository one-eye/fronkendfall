import React, { useEffect } from "react";
import { connect } from 'react-redux';

import './DisableUsers.css';
import UserPicker from '../components/UserPicker';
import { fetchPeople, toggleUser } from "../actions";
import { byName } from "../lib/UserUtils";

const DisableUsers = ({ people, toggleUser, loadUsers }) => {
  useEffect(() => {loadUsers()}, []);
  const peopleList = Object.values(people).sort(byName);
  const active = peopleList.filter(p => !p.disabled);
  const disabled = peopleList.filter(p => !!p.disabled);
  return (
    <div id="disable-users-page">
      <h1>Still fightin'! ({active.length})</h1>
      <UserPicker users={active} className="enabled" onUserSelect={(user) => toggleUser(user.id)} />
      <h1>...their deeds of valor will be remembered. ({disabled.length})</h1>
      <UserPicker users={disabled} className="disabled" onUserSelect={(user) => toggleUser(user.id)} />
    </div>
  )
};


const mapStateToProps = state => ({
  people: state.players.data,
});

const mapDispatchToProps = dispatch => ({
  toggleUser: id => dispatch(toggleUser(id)),
  loadUsers: () => dispatch(fetchPeople())
});

export default connect(mapStateToProps, mapDispatchToProps)(DisableUsers);
