import React from "react";
import { Link } from "react-router-dom";
import useTitle from '../useTitle';
import Suspense from "../components/Suspense";
import { getLatestLeagueTournament } from "../api/api";
import { toMonthDayAndYear } from "../util";

import './Home.css';
import logo from '../img/oem.svg';

const LatestTournament = ({ latestTournament }) => (
  <Link to={`/tournaments/${latestTournament.id}`} className="previous-tournament">
    <img src={latestTournament.cover} alt={`Poster for ${latestTournament.name}`} />
    <div className="info">
      <div className="header">{latestTournament.name}</div>
      <div className="date">{toMonthDayAndYear(latestTournament.scheduled)}</div>
    </div>
  </Link>
);

const Home = () => {
  useTitle('DrunkenFall');
  return (
    <div id="home-page">
      <div className="hero">
        <div className="logo">
          <img src={logo} alt="DrunkenFall" />
        </div>
        <div className="text">
          <p className="company">One Eye Productions</p>
          <p className="drunkenfall">DrunkenFall</p>
          <p className="tagline">Serious about not being serious</p>
          <p className="description">A Free-For-All TowerFall tournament</p>
        </div>
        <div className="cta">
          <Link to="/register" className="button">Join Now</Link>
        </div>
      </div>
      <div className="about">
        <div className="video">
          <h2>What is DrunkenFall?</h2>
          <iframe
            title="DrunkenFall 2019 - Teaser Trailer"
            src="https://www.youtube-nocookie.com/embed/8YqjoGHPQ9I"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen></iframe>
        </div>
        <div className="previous-tournaments">
          <h2>Our previous tournaments</h2>
          <Suspense loader={() => getLatestLeagueTournament()} propName="latestTournament" deps={[]}>
            <LatestTournament />
          </Suspense>
          <div className="cta">
            <Link to="/tournaments" className="button">View all</Link>
          </div>
        </div>
        <div className="rules">
          <h2>SEX!</h2>
          <h3>It's dangerous to go alone.</h3>

          <p>
            <b>Sorry, we needed your attention.</b> Here at DrunkenFall we have a special kind of ruleset
            you might not be familiar with if you’ve only played TowerFall vanilla.
          </p>

          <p>
            Please take a moment to familiarize yourself with the different modifiers and the
            implications for fuckups! It might save you during the tournament.
          </p>
          <div className="cta">
            <Link to="/rules" className="button">View Rules</Link>
          </div>
        </div>
        <div className="about-us">
          <h2>About us</h2>
          <p>
            DrunkenFall started as a way to play multiplayer and drink. Just a group of friends who
            want to play games and have a good time. It has over the last five years developed to
            recurring event with weeks of preparing, planning, and coding. The baseline is still
            there though - we're just a group of friends who want to play games and have a good
            time, join us!
          </p>

          <p>
            Our events are streamed on <a href="https://twitch.tv/drunkenfallofficial">our Twitch
            channel</a>, and you are most welcome to come join the fun, even if you are not
            physically present at the tournament!
          </p>

          <div className="cta">
            <Link to="/about" className="button">View more</Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Home;
