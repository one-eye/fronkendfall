import React, { useContext } from 'react';
import useTitle from "../useTitle";
import TournamentControls from "../components/TournamentControls";
import AuthenticationContext from "../components/AuthenticationContext";
import { getNextMatch, getNextScheduledMatch} from "../lib/TournamentUtils";
import logo from '../img/oem.svg';
import { getHex } from '../lib/ColorUtils';

import './RunningTournament.css';
import { getLevelName } from "../lib/LevelUtils";
import NextUpList from "../components/NextUpList";
import Countdown from "../components/Countdown";

const UpcomingPlayers = ({ title, players, ...rest }) => (
  <div className="next-scheduled" {...rest}>
    <h1>{ title }</h1>
    <div className="players">
      {
        players.map(player => (
          <div className="player" key={player.id}>
            <div className="nick" style={{color: getHex(player.color || 'white')}}>{player.nick || `Player with id ${player.person_id}`}</div>
          </div>
        ))
      }
    </div>
  </div>
);

const RunningTournament = ({tournament}) => {
  useTitle(`${tournament.tournament.title} - DrunkenFall`);
  const { user } = useContext(AuthenticationContext);
  const nextMatch = getNextMatch(tournament.matches)
    .getOrThrow(() => new Error('RunningTournament: Could not get next match.'));
  const nextScheduledMatch = getNextScheduledMatch(tournament.matches);
  return (
    <>
      <TournamentControls tournament={tournament} user={user}/>
      <div id="running-tournament-page">
        <div className="next-up">
          <h1>Next up!</h1>
          <NextUpList className="players" tournament={tournament} nextMatch={nextMatch} />
          <div className="match-info">
            <div className="name">{nextMatch.kind}</div>
            <div>Starts in</div>
            <div className="level">{getLevelName(nextMatch.level)}</div>
            <div className="eta"><Countdown to={nextMatch.scheduled}/></div>
          </div>
        </div>
        <div className="branding">
          <div className="logo">
            <img src={logo} alt="Drunkenfall Logo"/>
          </div>
          <div className="tournament">
            <div className="title">
              DrunkenFall
            </div>
            <div className="subtitle">
              {tournament.tournament.title}
            </div>
          </div>
        </div>
        <div className="upcoming">
          {
            nextScheduledMatch &&
            <UpcomingPlayers title="Next Scheduled" players={nextScheduledMatch.players}/>
          }
          {
            tournament.runnerups &&
            <UpcomingPlayers title="In queue" players={tournament.runnerups}/>
          }
        </div>
      </div>
    </>
  );
};
export default RunningTournament;
//
// const mapDispatchToProps = (dispatch) => ({
//   loadPeople: () => dispatch(fetchPeople()),
// });
// export default connect(
//   () => ({}),
//   mapDispatchToProps
// )(RunningTournament);
