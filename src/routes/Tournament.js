import React, { useEffect } from 'react';
import PastTournament from './PastTournament';
import { fetchParticipants, fetchPeople, fetchTournament, toggleParticipant } from "../actions";
import { connect } from "react-redux";
import Loading from "../components/Loading";
import RunningTournament from "./RunningTournament";
import ScheduledTournament from "./ScheduledTournament";
import { isEnded, isStarted } from "../lib/DateUtils";

const TournamentRenderer = ({ tournament, people, participants, toggleParticipant }) => {
  return (
    <>
      {
        isEnded(tournament.tournament) ? <PastTournament tournament={tournament}/> :
          isStarted(tournament.tournament) ? <RunningTournament tournament={tournament}/> :
          <ScheduledTournament tournament={tournament} people={people} participants={participants} toggleParticipant={toggleParticipant}/>
      }
    </>
  )
};

const Tournament = ({ tournament, people, participants, isLoading, error, loadTournament, loadParticipants, loadUsers, toggleParticipant }) => {
  useEffect(() => {
    loadTournament();
    loadUsers();
    loadParticipants();
  }, []);
  return (
    <div id="tournament-page">
      <Loading isLoading={isLoading} error={error}>
        {
          tournament && <TournamentRenderer tournament={tournament} people={people} participants={participants} toggleParticipant={toggleParticipant}/>
        }
      </Loading>
    </div>
  )
};

const mapStateToProps = (state, ownProps) => ({
  people: state.players.data,
  participants: state.participants.data[ownProps.match.params.id] || [],
  loading: state.tournamentDetails.loading || state.players.loading || state.participants.loading,
  error: state.tournamentDetails.error || state.players.error || state.participants.error,
  tournament: state.tournamentDetails.data[ownProps.match.params.id]
});

const mapDispatchToProps = (dispatch, props) => ({
  loadTournament: () => dispatch(fetchTournament(props.match.params.id)),
  loadUsers: () => dispatch(fetchPeople()),
  loadParticipants: () => dispatch(fetchParticipants(props.match.params.id)),
  toggleParticipant: (userId) => dispatch(toggleParticipant(props.match.params.id, userId)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tournament);
