import React, { useContext } from 'react';
import './Login.css';
import AuthenticationContext from '../components/AuthenticationContext';
import { Redirect } from "react-router-dom";
import Button from '../components/Button';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

const Login = ({ location: { state } }) => {

  const { from } = state || { from: '/' };
  const { user } = useContext(AuthenticationContext);

  return (
    <div id="login-page">
      {
        user !== undefined ? <Redirect to={from} /> :
          <form>
            <Button onClick="/api/auth/discord/" type="external" icon={faArrowRight}>Login with Discord</Button>
          </form>
      }
    </div>
  )
}

export default Login;
