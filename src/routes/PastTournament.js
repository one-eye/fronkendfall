import React from 'react';
import './PastTournament.css';
import { toLocaleDate, getWinnerOfTournament, getStatsOfPlayer, getLosersOfTournament, pointComparator } from '../lib/TournamentUtils';
import Suspense from '../components/Suspense';
import Avatar from '../components/Avatar';
import { getArcher } from "../api/api";

const WinnerStats = ({ stats }) => (
  <table className="stats">
    <tbody>
      <tr><td>Kills</td><td>{stats.kills}</td></tr>
      <tr><td>Self Kills</td><td>{stats.self}</td></tr>
      <tr><td>Shots</td><td>{stats.shots}</td></tr>
      <tr><td>Sweeps</td><td>{stats.sweeps}</td></tr>
    </tbody>
  </table>
);

const Name = ({ person }) => <>{person.nick}</>
const LazyNick = ({ personId }) => (
  <Suspense
    loader={() => getArcher(personId).catch(() => ({ nick: '¯\\_(ツ)_/¯' }))}
    deps={[personId]}
    propName="person"
  >
    <Name />
  </Suspense>
);

const Scrubs = ({ playerSummaries }) => (
  <table className="players">
    <thead>
      <tr>
        <td className="nick">Nick</td>
        <td>Kills</td>
        <td>Self</td>
        <td>Shots</td>
        <td>Sweeps</td>
      </tr>
    </thead>
    <tbody>
      {
        playerSummaries.map(ps => (
          <tr key={ps.id}>
            <td className="nick"><LazyNick personId={ps.person_id} /></td>
            <td>{ps.kills}</td>
            <td>{ps.self}</td>
            <td>{ps.shots}</td>
            <td>{ps.sweeps}</td>
          </tr>
        ))
      }
    </tbody>
  </table>
);

const MatchPlayer = ({ player }) => (
  <tr className={`player ${player.color}`}>
    <td className="name">
      {
        player.display_names ? player.display_names.join(" ") :
          <LazyNick personId={player.person_id} />
      }
    </td>
    <td>{player.kills}</td>
    <td>{player.self}</td>
    <td>{player.shots}</td>
    <td>{player.sweeps}</td>
  </tr>
);

const Match = ({ match }) => (
  <>
    <h3>{match.kind}</h3>
    <table className="match-players">
      <thead>
        <tr>
          <td className="nick">Nick</td>
          <td><span role="img" aria-label="Kills">☠️</span></td>
          <td><span role="img" aria-label="Self Kills">🤯🔫</span></td>
          <td><span role="img" aria-label="Shots">🍺</span></td>
          <td><span role="img" aria-label="Sweeps">🧹</span></td>
        </tr>
      </thead>
      <tbody>
        {
          match.players
            .sort(pointComparator)
            .map(player => <MatchPlayer key={player.id} player={player} />)
        }
      </tbody>
    </table>
  </>
);

const PastTournament = ({ tournament }) => {
  const winner = getWinnerOfTournament(tournament);
  const losers = getLosersOfTournament(tournament, winner.person_id);
  return (
    <div id="past-tournament-page">
      <div className="section hero">
        <div className="image">
          <img src={tournament.tournament.cover} alt={tournament.tournament.name} />
        </div>
        <div className="text">
          <h1>
            {tournament.tournament.name}
          </h1>
          <p>{toLocaleDate(tournament.tournament.started)}</p>
        </div>
      </div>
      <div className="section winner">
        <h2>The Winner</h2>
        <Suspense loader={() => getArcher(winner.person_id)} deps={[winner.person_id]} propName="archer">
          <Avatar />
        </Suspense>
        <WinnerStats stats={getStatsOfPlayer(tournament, winner.person_id)} />
      </div>
      <div className="section scrubs">
        <h2>The scrubs</h2>
        <Scrubs playerSummaries={losers} />
      </div>
      {
        tournament.matches.reverse().map(match => (
          <div className="section" key={match.id}>
            <Match match={match} />
          </div>
        ))
      }
    </div>
  )
}

export default PastTournament;
