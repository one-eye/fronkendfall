import React, { useEffect } from "react";
import { connect } from 'react-redux';

import './Participants.css';
import UserPicker from '../components/UserPicker';
import { fetchParticipants, fetchPeople, fetchTournament, toggleParticipant } from "../actions";
import { byName } from "../lib/UserUtils";
import TournamentControls from "../components/TournamentControls";

const Participants = ({ match: { params: { id: tournamentId } }, people, participants, toggleParticipant, loadUsers, loadParticipants, loadTournament, loggedInUser, loading, error, tournament}) => {
  useEffect(() => loadUsers(), []);
  useEffect(() => loadParticipants(tournamentId), []);
  useEffect(() => loadTournament(tournamentId), []);
  if (loading) return null;
  const participating = participants.map(p => people[p.person_id]);
  const participantIds = participating.reduce((p,c) => {p[c.id] = true; return p}, {});
  const sittingOut = Object.values(people).filter(p => !participantIds[p.id] && !p.disabled).sort(byName);
  return (
    <div id="participants-page">
      {
        !(loading || error) && tournament && <TournamentControls tournament={tournament} user={loggedInUser} />
      }
      <h1>In for the showdown! |o/ ({participating.length})</h1>
      <UserPicker users={participating} className="participating" onUserSelect={(user) => toggleParticipant(tournamentId, user.id)} />
      <h1>Booooooo 😧 ({sittingOut.length})</h1>
      <UserPicker users={sittingOut} className="sitting-out" onUserSelect={(user) => toggleParticipant(tournamentId, user.id)} />
    </div>
  )
};


const mapStateToProps = (state, ownProps) => ({
  people: state.players.data,
  participants: state.participants.data[ownProps.match.params.id] || [],
  loading: state.tournamentDetails.loading || state.players.loading || state.participants.loading,
  error: state.tournamentDetails.error || state.players.error || state.participants.error,
  tournament: state.tournamentDetails.data[ownProps.match.params.id]
});

const mapDispatchToProps = dispatch => ({
  toggleParticipant: (tournamentId, userId) => dispatch(toggleParticipant(tournamentId, userId)),
  loadUsers: () => dispatch(fetchPeople()),
  loadParticipants: id => dispatch(fetchParticipants(id)),
  loadTournament: id => dispatch(fetchTournament(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Participants);
